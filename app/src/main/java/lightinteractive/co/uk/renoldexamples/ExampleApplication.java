package lightinteractive.co.uk.renoldexamples;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import lightinteractive.co.uk.renoldexamples.service.BLEManagerService;

/**
 * Created by colinlight on 07/12/2017.
 */

public class ExampleApplication extends Application {

    private BLEManagerService.BLEServiceBinder mServiceBinder;

    @Override
    public void onCreate(){
        super.onCreate();
        //bind the service to the application class, this will prevent the
        //Android system destroying it.
        //See main Activity to see how to bind an Activity to the service and communicate with it.
        this.bindService(new Intent(this.getApplicationContext(), BLEManagerService.class),
                mServiceConnection, BIND_AUTO_CREATE);

    }

    @Override
    public void onTerminate(){
        super.onTerminate();
        this.unbindService( mServiceConnection );
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mServiceBinder = (BLEManagerService.BLEServiceBinder)service;
            mServiceBinder.initialize();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };


}
