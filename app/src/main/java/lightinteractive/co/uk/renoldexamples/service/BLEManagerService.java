package lightinteractive.co.uk.renoldexamples.service;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by colinlight on 07/12/2017.
 */

public class BLEManagerService extends Service {

    //Define broadcast events here Activities can subscribe to these
    //and you will dispatch them from your service
    public final static String ACTION_BLE_DEVICE_CONNECTED = "com.renold.smartlink.bluetooth.ACTION_BLE_DEVICE_CONNECTED";

    private static final String TAG = BLEManagerService.class.getSimpleName();

    private BLEServiceBinder mBinder;

    /**
     * BLEServiceBinder
     * <p>
     * Service public interface class enables communication with the service
     */
    public class BLEServiceBinder extends Binder {

        private BLEManagerService mService;

        //constructor the service is passed in for reference
        //and objects binding to it can get the service from here
        public BLEServiceBinder(BLEManagerService service) {
            mService = service;
        }

        public BLEManagerService getService() {
            return mService;
        }


        public boolean initialize() {
            return true;
        }
        //put public calls to the service here e.g. this is it's API
        //an example might be as below
        public void connectToBLEDevice(BluetoothDevice device ){
            Log.v(TAG, "connectToBLEDevice called");

            //we can do the connection stuff and when done notifiy any listening objects
            //by dispatching a broadcast event
            broadcastUpdate( ACTION_BLE_DEVICE_CONNECTED );
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        //objects ( Activities ) call this to bind to the service
        if (mBinder == null) {
            mBinder = new BLEServiceBinder(this);
        }
        return mBinder;
    }


    @Override
    public boolean onUnbind(Intent intent) {
        //objects ( Activities ) call this to unbind from the service
        return super.onUnbind(intent);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       //kill any threads etc here and do clean up
    }


    /**
     * Send a broadcast action/event to any object subscribing to the service intents
     * @param action
     */
    private void broadcastUpdate(final String action) {
        final Intent broadcastItent = new Intent(action);
        sendBroadcast(broadcastItent);
    }

    /**
     * Activities can get the service intent filters here and subscribe to them
     */
    public static IntentFilter getServiceIntentFilters() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BLEManagerService.ACTION_BLE_DEVICE_CONNECTED);
        //you can add other broadcast events here
        return intentFilter;
    }


}
