package lightinteractive.co.uk.renoldexamples.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import lightinteractive.co.uk.renoldexamples.R;
import lightinteractive.co.uk.renoldexamples.service.BLEManagerService;

public class MainActivity extends AppCompatActivity {

    private BLEManagerService.BLEServiceBinder mBLEServiceBinder;
    private ServiceConnection mServiceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Connect to the Android service
        connectToBLEService();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Unbind from the service
        unbindService(mServiceConnection);
        //unregister for service intents
        mBLEServiceBinder.getService().unregisterReceiver(mServiceUpdateReceiver);
    }


    /**
     * Set up a service callback listener then bind to the service
     */
    private void connectToBLEService() {
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mBLEServiceBinder = (BLEManagerService.BLEServiceBinder) service;
                //we shouldn't need to call this as it's done in the ExampleApplication class
                mBLEServiceBinder.initialize();

                //we can now make calls on the Android service

                //we register for service updates, pass in the mServiceUpdateReceiver which is defined
                //below and handles the events. We get the complete list of service intents to subscribe to
                // using BLEManagerService.getServiceIntentFilters()
                mBLEServiceBinder.getService().registerReceiver(mServiceUpdateReceiver,
                        BLEManagerService.getServiceIntentFilters() );

                //we can now make a call on the service so for example if you wanted to
                //connect to ble devices we could use the device selected in ScanResult and
                //call connectBLEDevice( device );
                mBLEServiceBinder.connectToBLEDevice( null );
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBLEServiceBinder = null;
            }
        };
        //bind to the service passing in the service connection callback
        this.bindService(new Intent(this, BLEManagerService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);

    }


    /**
     * Handles various events fired by the Service.
     * */
    private BroadcastReceiver mServiceUpdateReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BLEManagerService.ACTION_BLE_DEVICE_CONNECTED.equals(action)) {
                Log.v( "BroadcastReceiver", "...............ACTION_GATT_CONNECTED" );

               displayConnectionAlert();
                return;
            }
        }
    };

    private void displayConnectionAlert(){
        AlertDialog.Builder dlgbuiler = new AlertDialog.Builder(this);
        dlgbuiler.setTitle("Connected!");
        dlgbuiler.setMessage("Device Connected via an Android serivce, tap screen to remove");
        AlertDialog dlg=dlgbuiler.create();
        dlg.show();

    }
}
